from __future__ import division
import cv2, numpy as np
from keras.models import Model
from keras.layers import Input
from keras.layers.core import Activation, Reshape, Permute
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D, Deconvolution2D
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.normalization import BatchNormalization


def plot_model(model_to_plot):
    from keras.utils.visualize_util import plot

    print '[INFO] Plotting model to png ...'
    plot(model_to_plot, show_shapes=True,
         to_file='model.png')

def build_model(image_shape, nb_class):
    # Input
    image_input = Input(shape=image_shape, name='image_input')

    """ Conv 1 """
    x = ZeroPadding2D((1, 1))(image_input)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = Convolution2D(64, 3, 3, name='conv1_1')(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = Activation('relu')(x)
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(64, 3, 3, name='conv1_2')(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = Activation('relu')(x)
    x = MaxPooling2D((2, 2), strides=(2, 2))(x)

    """ Conv 2 """
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(128, 3, 3, name='conv2_1')(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = Activation('relu')(x)
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(128, 3, 3, name='conv2_2')(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = Activation('relu')(x)
    x = MaxPooling2D((2, 2), strides=(2, 2))(x)

    """ Conv 3 """
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(256, 3, 3, name='conv3_1')(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = Activation('relu')(x)
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(256, 3, 3, name='conv3_2')(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = Activation('relu')(x)
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(256, 3, 3, name='conv3_3')(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = Activation('relu')(x)
    x = MaxPooling2D((2, 2), strides=(2, 2))(x)

    """ Conv 4 """
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(512, 3, 3, name='conv4_1')(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = Activation('relu')(x)
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(512, 3, 3, name='conv4_2')(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = Activation('relu')(x)
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(512, 3, 3, name='conv4_3')(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = Activation('relu')(x)
    x = MaxPooling2D((2, 2), strides=(2, 2))(x)

    """ Conv 5 """
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(512, 3, 3, name='conv5_1')(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = Activation('relu')(x)
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(512, 3, 3, name='conv5_2')(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = Activation('relu')(x)
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(512, 3, 3, name='conv5_3')(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = Activation('relu')(x)
    x = MaxPooling2D((2, 2), strides=(2, 2))(x)

    """ DeConv 5 """
    x = Deconvolution2D(512, 3, 3, subsample=(2, 2), dim_ordering='th', output_shape=(None, 512, int(image_shape[1]/16), int(image_shape[2]/16)),
                              border_mode='same', init='he_normal')(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(512, 3, 3)(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(512, 3, 3)(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(512, 3, 3)(x)
    x = BatchNormalization(mode=0, axis=1)(x)

    """ DeConv 4 """
    x = Deconvolution2D(512, 3, 3, subsample=(2, 2), dim_ordering='th', output_shape=(None, 512, int(image_shape[1]/8), int(image_shape[2]/8)),
                              border_mode='same', init='he_normal')(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(512, 3, 3)(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(512, 3, 3)(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(512, 3, 3)(x)
    x = BatchNormalization(mode=0, axis=1)(x)

    """ DeConv 3 """
    x = Deconvolution2D(256, 3, 3, subsample=(2, 2), dim_ordering='th', output_shape=(None, 256, int(image_shape[1]/4), int(image_shape[2]/4)),
                              border_mode='same', init='he_normal')(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(256, 3, 3)(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(256, 3, 3)(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(256, 3, 3)(x)
    x = BatchNormalization(mode=0, axis=1)(x)

    """ DeConv 2 """
    x = Deconvolution2D(128, 3, 3, subsample=(2, 2), dim_ordering='th', output_shape=(None, 128, int(image_shape[1]/2), int(image_shape[2]/2)),
                        border_mode='same', init='he_normal')(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(128, 3, 3)(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(128, 3, 3)(x)
    x = BatchNormalization(mode=0, axis=1)(x)

    """ DeConv 1 """
    x = Deconvolution2D(64, 3, 3, subsample=(2, 2), dim_ordering='th', output_shape=(None, 64, image_shape[1], image_shape[2]),
                        border_mode='same', init='he_normal')(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = ZeroPadding2D((1, 1))(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = Convolution2D(64, 3, 3)(x)
    x = BatchNormalization(mode=0, axis=1)(x)
    x = ZeroPadding2D((1, 1))(x)
    x = Convolution2D(64, 3, 3)(x)
    x = BatchNormalization(mode=0, axis=1)(x)

    # Output
    x = Convolution2D(nb_class, 1, 1, border_mode='same', init='he_normal', name='image_final_conv')(x)                 # (None, nb_class, 224, 224)
    x = Reshape(target_shape=(nb_class, image_shape[1]*image_shape[2]),
                input_shape=(nb_class, image_shape[1], image_shape[2]))(x)                                              # (None, nb_class, 224*224)
    x = Permute((2, 1))(x)                                                                                              # (None, 224*224, nb_class)
    x = Activation('softmax')(x)                                                                                        # (None, 224*224, nb_class)
    image_output = Permute((2, 1), name='image_output')(x)                                                              # (None, nb_class, 224*224)

    model = Model(input=image_input, output=image_output); model.summary()

    return model
