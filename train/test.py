from __future__ import division
import sys, argparse, cv2
import numpy as np
import scipy.io as sio
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from model_test import build_model
from scipy.misc import imresize
from keras.optimizers import Adam
from custom_loss import image_categorical_crossentropy


# Hyperparameter
image_shape = (3, 224*4, 224*4)
workspace_path = '/home/dkkim930122/Desktop/to_transfer/train/'# Please change this according to your workspace path
TRAIN_INPUT_MEAN_IMAGE = np.array([92.4679, 96.6908, 93.6305])
TRAIN_INPUT_STD_IMAGE  = np.array([30.551, 33.6411, 34.3133])

def data_process(rgb_image_crop):
    # Postprocess (Mean subtraction and unit variance)
    rgb_image_crop[0, 0,:,:] = rgb_image_crop[0, 0,:,:] - TRAIN_INPUT_MEAN_IMAGE[0]
    rgb_image_crop[0, 1,:,:] = rgb_image_crop[0, 1,:,:] - TRAIN_INPUT_MEAN_IMAGE[1]
    rgb_image_crop[0, 2,:,:] = rgb_image_crop[0, 2,:,:] - TRAIN_INPUT_MEAN_IMAGE[2]

    rgb_image_crop[0, 0,:,:] = rgb_image_crop[0, 0,:,:] / TRAIN_INPUT_STD_IMAGE[0]
    rgb_image_crop[0, 1,:,:] = rgb_image_crop[0, 1,:,:] / TRAIN_INPUT_STD_IMAGE[1]
    rgb_image_crop[0, 2,:,:] = rgb_image_crop[0, 2,:,:] / TRAIN_INPUT_STD_IMAGE[2]

    return rgb_image_crop

def save_result(rgb_image, pred_image, pred_count_image, iteration, counter):
    save_image = np.zeros((rgb_image.shape[1], rgb_image.shape[2]))

    for row_save in range(rgb_image.shape[1]):
        for col_save in range(rgb_image.shape[2]):
    	    if pred_count_image[row_save, col_save] > 0:
    	        save_image[row_save, col_save] = pred_image[row_save, col_save] / pred_count_image[row_save, col_save]
    
    save_image = save_image - np.min(save_image.flatten())
    save_image = save_image / np.max(save_image.flatten())
    
    sio.savemat(workspace_path + 'test_iter_' + str(iteration) + '.mat', {'x': save_image, 'y': pred_image, 'z': pred_count_image})

    fig = plt.figure(frameon=False)
    dpi = 80; fig.set_size_inches(save_image.shape[1]/dpi, save_image.shape[0]/dpi)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    ax.imshow(save_image, cmap="Spectral", vmin=0, vmax=1, aspect='normal', alpha=1.0)
    plt.axis('off')
    fig.savefig(workspace_path + 'test_iter_' + str(iteration) + '_' + str(counter) + '.png', dpi=dpi)
    plt.close()

def main(data_path, nb_class):
    iteration = 2750

    """ Model """
    print '[ INFO ] Loading model ...'
    network = build_model(image_shape=image_shape, nb_class=nb_class)
    network.load_weights(workspace_path + 'weight/finetune_trainModel_iter' + str(iteration) + '.h5')
    network.compile(loss={'image_output': image_categorical_crossentropy}, optimizer=Adam())

    """ Image Setting """
    # Read RGB image
    rgb_image = cv2.imread(data_path + 'rgb_image.png', cv2.IMREAD_COLOR)
    rgb_image = np.transpose(rgb_image, (2, 0, 1))# Transpose: (Ch, row, col)
    rgb_image = np.asarray(rgb_image, dtype=np.float32)# NOTE data_type change in here (the dtype reads in uint8, which cannot be negative int)
    print 'rgb_image.shape: {}'.format(rgb_image.shape)# (6100, 4720, 3)

    # Pred image
    pred_image = np.zeros((rgb_image.shape[1], rgb_image.shape[2]))
    pred_count_image = np.zeros((rgb_image.shape[1], rgb_image.shape[2]))

    """ Prediction """
    counter = 0
    for row in range(0, rgb_image.shape[1]-image_shape[1], 56*4):
        for col in range(0, rgb_image.shape[2]-image_shape[2], 28*2):
            print '[ STATUS ] Current counter: {} (row: {}:{}, col{}:{}'.format(counter, row, row+image_shape[1], col, col+image_shape[2])

            test_image = np.zeros((1, image_shape[0], image_shape[1], image_shape[2]), dtype=np.float32)
            test_image[0, :, :, :] = rgb_image[:, row:row+image_shape[1], col:col+image_shape[2]]
            test_image = data_process(test_image)

            # Prediction
            image_prediction = network.predict_on_batch(test_image)
            image_prediction = image_prediction[0, 1, :]
            image_prediction = np.reshape(image_prediction, (image_shape[1], image_shape[2]))

            # Save
            pred_image[row:row+image_shape[1], col:col+image_shape[2]] += image_prediction
            pred_count_image[row:row+image_shape[1], col:col+image_shape[2]] += 1

            counter = counter + 1

            save_result(rgb_image, pred_image, pred_count_image, iteration, counter)

    save_result(rgb_image, pred_image, pred_count_image, iteration, counter)

    del network

if __name__ == '__main__':
    """ Input argument """
    # Argument parser
    parser = argparse.ArgumentParser(description='VoxSegNet')
    parser.add_argument('-d', '--data_path', default=None, help='Specify path to where rosbags are located.')
    parser.add_argument('-n', '--num_class', default=None, help='Number of class in label.')
    args = parser.parse_args()

    # Check input
    if args.data_path==None or args.num_class==None:
        raise RuntimeError('[ ERROR ] INSUFFICIENT INPUT. PLEASE DOUBLE CHECK INPUT')
    else:
        args.num_class = int(args.num_class)
        print '[ INFO ] data_path:', args.data_path, ', num_class:', args.num_class
        main(data_path=args.data_path, nb_class=args.num_class)
