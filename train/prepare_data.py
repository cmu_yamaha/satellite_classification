from __future__ import division
import glob, os, random, cv2, h5py
import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt
from scipy.misc import imresize


# Hyper parameter
IMAGE_DATA_SHAPE = np.array([3, 224, 224])  # (channel, row, col)
TRAIN_INPUT_MEAN_IMAGE = np.array([92.4679, 96.6908, 93.6305])
TRAIN_INPUT_STD_IMAGE  = np.array([30.551, 33.6411, 34.3133])

def shuffle(list_to_shuffle):
    random.seed(42)
    random.shuffle(list_to_shuffle)

    return list_to_shuffle

def load_data_list(data_path):
    print '[ INFO ] Loading data list ...'

    # train_list
    train_list_to_return = []

    os.chdir(data_path + 'v2/train/rgb/')
    for raw_item in glob.glob("*.png"):
        timestamp  = raw_item[0:10]
        rgb_item   = 'v2/train/rgb/' + timestamp + '_rgb.png'
        label_item = 'v2/train/label/' + timestamp + '_label.png'

        # Append
        train_list_to_return.append((rgb_item) + " " + (label_item))

    os.chdir(data_path + 'v3/train/rgb/')
    for raw_item in glob.glob("*.png"):
        timestamp  = raw_item[0:10]
        rgb_item   = 'v3/train/rgb/' + timestamp + '_rgb.png'
        label_item = 'v3/train/label/' + timestamp + '_label.png'

        # Append
        train_list_to_return.append((rgb_item) + " " + (label_item))

    # Shuffle before return
    return shuffle(train_list_to_return)

def data_process(data_path, item_path, option, num_class):
    if option == 'image_input':
        # Read image
        rgb_image = cv2.imread(data_path + item_path, cv2.IMREAD_COLOR)
        rgb_image = np.transpose(rgb_image, (2, 0, 1))# Transpose: (Ch, row, col)
        rgb_image = np.asarray(rgb_image, dtype=np.float32)# NOTE data_type change in here (the dtype reads in uint8, which cannot be negative int)

        # Postprocess (Mean subtraction and unit variance)
        rgb_image[0,:,:] = rgb_image[0,:,:] - TRAIN_INPUT_MEAN_IMAGE[0]
        rgb_image[1,:,:] = rgb_image[1,:,:] - TRAIN_INPUT_MEAN_IMAGE[1]
        rgb_image[2,:,:] = rgb_image[2,:,:] - TRAIN_INPUT_MEAN_IMAGE[2]

        rgb_image[0,:,:] = rgb_image[0,:,:] / TRAIN_INPUT_STD_IMAGE[0]
        rgb_image[1,:,:] = rgb_image[1,:,:] / TRAIN_INPUT_STD_IMAGE[1]
        rgb_image[2,:,:] = rgb_image[2,:,:] / TRAIN_INPUT_STD_IMAGE[2]

        return rgb_image

    elif option == 'image_output':
        # Read image
        label_image = cv2.imread(data_path + item_path, cv2.IMREAD_GRAYSCALE)

        # Label correction
        label_corrected_image = np.zeros((IMAGE_DATA_SHAPE[1], IMAGE_DATA_SHAPE[2]), dtype=np.uint8)
        for row in range(IMAGE_DATA_SHAPE[1]):
            for col in range(IMAGE_DATA_SHAPE[2]):
                if label_image[row, col] > 100:
                    label_corrected_image[row, col] = 1

        flatten_image = label_corrected_image.flatten()# Flatten label_corrected_image

        # one_hot
        one_hot = np.zeros((num_class, IMAGE_DATA_SHAPE[1]*IMAGE_DATA_SHAPE[2]), dtype=np.uint8)
        for class_index in range(num_class):
            valid_ind = np.where(flatten_image == class_index)
            one_hot[class_index, valid_ind] = 1

        if (np.sum(one_hot.flatten()) != IMAGE_DATA_SHAPE[1]*IMAGE_DATA_SHAPE[2]):
            raise RuntimeError('[ ERROR ] WRONG ONE HOT')

        return one_hot

    else:
        raise RuntimeError('[ ERROR ] data_process::Wrong option!')

def calculate_train_mean_std(data_path, num_class):
    print '[ INFO ] Calculating train mean ...'

    # Load data list
    train_list = load_data_list(data_path)

    # Initialize
    number_of_sample = len(train_list)
    train_image = np.zeros((number_of_sample, IMAGE_DATA_SHAPE[0], IMAGE_DATA_SHAPE[1], IMAGE_DATA_SHAPE[2]), dtype=np.float32)

    # Put data into train_image
    for index in range(number_of_sample):
        current_item = train_list[index]; current_item = current_item.split()
        print index, current_item[0], current_item[1]

        # Input
        train_image[index, :, :, :] = data_process(data_path, current_item[0], 'image_input', num_class)

    # Calculate and display
    train_mean = np.mean(train_image[:, 0, :, :]); print 'TRAIN_INPUT_MEAN (R):', train_mean
    train_mean = np.mean(train_image[:, 1, :, :]); print 'TRAIN_INPUT_MEAN (G):', train_mean
    train_mean = np.mean(train_image[:, 2, :, :]); print 'TRAIN_INPUT_MEAN (B):', train_mean

    train_std = np.std(train_image[:, 0, :, :]); print 'TRAIN_INPUT_STD (R) :', train_std
    train_std = np.std(train_image[:, 1, :, :]); print 'TRAIN_INPUT_STD (G) :', train_std
    train_std = np.std(train_image[:, 2, :, :]); print 'TRAIN_INPUT_STD (B) :', train_std

def calculate_train_class_frequency(data_path, num_class):
    print '[ INFO ] Calculating class weight ...'

    # Load data list
    train_list = load_data_list(data_path)

    # Initialize
    number_of_sample = len(train_list)
    train_output = np.zeros((num_class, 1*IMAGE_DATA_SHAPE[1]*IMAGE_DATA_SHAPE[2]))
    class_frequency = np.zeros((number_of_sample, num_class), dtype=np.uint64)

    # Loop over
    for train_index in range(number_of_sample):
        current_list = train_list[train_index]; current_list = current_list.split()
        print train_index, current_list[1]

        train_output = data_process(data_path, current_list[1], 'image_output', num_class)

        # Compute frequency
        for class_index in range(num_class):
            class_frequency[train_index, class_index] = np.count_nonzero(train_output[class_index, :])

    print np.mean(class_frequency, axis=0)

def return_batch(data_list, iteration, batch_size, data_path, nb_class):
    # Initialize
    image_batch_input  = np.zeros((batch_size, IMAGE_DATA_SHAPE[0], IMAGE_DATA_SHAPE[1], IMAGE_DATA_SHAPE[2]), dtype=np.float32)
    image_batch_output = np.zeros((batch_size, nb_class, IMAGE_DATA_SHAPE[1]*IMAGE_DATA_SHAPE[2]), dtype=np.uint8)

    # Construct batch
    for batch in range(batch_size):
        current_list = data_list[iteration*batch_size + batch]; current_list = current_list.split()

        image_batch_input[batch, :, :, :] = data_process(data_path, current_list[0], 'image_input', nb_class)
        image_batch_output[batch, :, :]   = data_process(data_path, current_list[1], 'image_output', nb_class)

    return image_batch_input, image_batch_output
