from __future__ import division
from __future__ import absolute_import
import theano.tensor as T
import numpy as np


def image_categorical_crossentropy(y_true, y_pred):
    epsilon = 1.0e-15
    weight = np.array([1.0, 19.13])

    # Clip (limit) the values in an array.
    y_pred = T.clip(y_pred, epsilon, 1.0 - epsilon)

    # Scale preds so that the class probas of each sample sum to 1
    y_pred /= y_pred.sum(axis=1, keepdims=True)

    cce = T.nnet.categorical_crossentropy(y_pred, y_true)

    return (weight*cce).mean()
