from __future__ import division
import argparse, prepare_data, logging
import numpy as np
from model import build_model
from keras.optimizers import Adam
from custom_loss import image_categorical_crossentropy


# Hyperparameter
image_shape = (3, 224, 224)
workspace_path = '/home/dk683/global_classification/train/keras/segnet/v4/'
logging.basicConfig(filename=workspace_path + 'loss.log', level=logging.DEBUG)# Log

def main(lr, batch_size, data_path, nb_class):
    """ Data prepare """
    train_list = prepare_data.load_data_list(data_path=data_path)
    print '[INFO] train_list len {}'.format(len(train_list))
    logging.info('[INFO] train_list len {}'.format(len(train_list)))

    """ Model """
    print '[ INFO ] Loading model ...'
    network = build_model(image_shape=image_shape, nb_class=nb_class)
    network.load_weights(workspace_path + 'weight/modified_vgg16_weights.h5', by_name=True)
    network.compile(loss={'image_output': image_categorical_crossentropy}, optimizer=Adam(lr=lr))

    """ Train """
    print '[ INFO ] Train starts :-)'
    max_epoch = 35
    max_iter = max_epoch*(len(train_list)/batch_size); max_iter = int(np.round(max_iter))
    start_iter, train_iter = 0, 0

    for iteration in range(start_iter, max_iter, 1):
        print 'Current iteration {}/{}'.format(iteration, max_iter)
        logging.info('Current iteration {}/{}'.format(iteration, max_iter))

        # Get train_iter, val_iter
        if train_iter >= np.floor(len(train_list)/batch_size):
            train_iter = 0

        # Train and validation loss
        train_image_input_batch, train_image_label_batch = \
        prepare_data.return_batch(train_list, iteration=train_iter, batch_size=batch_size, data_path=data_path, nb_class=nb_class)

        train_hist = network.train_on_batch({'image_input': train_image_input_batch}, {'image_output': train_image_label_batch})
        print 'Train Loss: {}'.format(train_hist); logging.info('Train Loss: {}'.format(train_hist))
        train_iter += 1

        """ Evaluation """
        if iteration%125 == 0:
            # Save model every x
            network.save_weights(workspace_path + 'weight/finetune_trainModel_iter' + str(iteration) + '.h5')

if __name__ == '__main__':
    """ Input argument """
    # Argument parser
    parser = argparse.ArgumentParser(description='VoxSegNet')
    parser.add_argument('-r', '--lr', default=None, help='Learning rate')
    parser.add_argument('-b', '--batch_size', default=None, help='Batch Size')
    parser.add_argument('-d', '--data_path', default=None, help='Specify path to where rosbags are located.')
    parser.add_argument('-n', '--num_class', default=None, help='Number of class in label.')
    args = parser.parse_args()

    # Check input
    if args.lr==None or args.batch_size==None or args.data_path==None or args.num_class==None:
        raise RuntimeError('[ ERROR ] INSUFFICIENT INPUT. PLEASE DOUBLE CHECK INPUT')
    else:
        args.batch_size, args.num_class, args.lr = int(args.batch_size), int(args.num_class), float(args.lr)
        print '[ INFO ] lr: {}, batch_size: {}, data_path: {}, num_class: {}'.format(args.lr, args.batch_size, args.data_path, args.num_class)
        logging.info('[ INFO ] lr: {}, batch_size: {}, data_path: {}, num_class: {}'.format(args.lr, args.batch_size, args.data_path, args.num_class))
        main(lr=args.lr, batch_size=args.batch_size, data_path=args.data_path, nb_class=args.num_class)
