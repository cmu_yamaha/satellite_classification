# SATELLITE CLASSIFICATION #
This repo has source code for the satellite classification.

### DATA ###
* Version 1 (v1): This is first version of an experiment. Entire Gascola area is divided into three non-overlapping areas (train, val, and test), and the test area is not the entire Gascola site. The label data is coming from Bill's data.
* Version 2 (v2): For the second version of the experiment, the setup is similar to the version 1, but the label data is modified. Powei's label data and my label data is added to the Bill's data.
* Version 3 (v3): This is the final version of the experiment that was shown in the presentation. I've enlarged the training data by considering more Gascolar area. The testing area is considered the entire Gascolar area. 

### TRAIN ###
* Source codes are in `train` folder.

### TEST ###
* Please view `train/test.py` file.
* Example commaind: `python test.py -d /path/to/data/v3/ -n 2`

### CONTACT ###
Dong-Ki Kim
dongkikim93@gmail.com
