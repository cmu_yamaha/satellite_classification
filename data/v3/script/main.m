clc; clear all; close all; warning('off','all');

%% Hyper-parameter
workspace_path        = '/home/dkkim930122/research/air_lab/yamaha/global_classification/data/v3/';% Please change this to according to your system
bn_threshold          = 1;% Binarize threshold
number_of_train_data  = 2000;
crop_size             = 224;% In pixel size (1 pixel corresponds to 0.3 meter)
train_row_start_vec   = [2453, 2327, 1730, 2700];% In index
train_row_end_vec     = [2770, 2645, 2065, 4000];% In index
train_col_start_vec   = [1707, 2195, 2480, 2750];% In index
train_col_end_vec     = [2100, 2515, 2960, 3650];% In index

%% Make rgb_train_image, label_train_image & rgb_val_image, label_val_image
% Read data
rgb_image   = imread(strcat(workspace_path, 'rgb_image.png'));
label_image = imread(strcat(workspace_path, 'label_image.png'));

%% Make folder for saving data
mkdir(strcat(workspace_path, 'train'));
mkdir(strcat(workspace_path, 'train/rgb'));
mkdir(strcat(workspace_path, 'train/label'));

%% Make train data
% NOTE: train data is collected at random location and random orientation
train_data_count = 1;

while train_data_count <= number_of_train_data
    % Generate random index and set a local patch index 
    index = randi([1, 4], 1);
    
    train_row_start = train_row_start_vec(index);
    train_row_end   = train_row_end_vec(index);
    train_col_start = train_col_start_vec(index);
    train_col_end   = train_col_end_vec(index);

    % According to the local patch index, set rgb and label train image
    rgb_train_image   = rgb_image(train_row_start:train_row_end, train_col_start:train_col_end, :);
    label_train_image = label_image(train_row_start:train_row_end, train_col_start:train_col_end);
    
    % Generate random number
    [resize_row, resize_col, ~] = size(rgb_train_image);

    row         = rand(1)*resize_row;
    col         = rand(1)*resize_col;
    orientation = rand(1)*360;

    % Set crop parameter
    bigger_crop_size = crop_size*sqrt(2);

    row_begin = row - bigger_crop_size/2;
    row_end   = row + bigger_crop_size/2;
    col_begin = col - bigger_crop_size/2;
    col_end   = col + bigger_crop_size/2;

    % Check margin
    if row_begin >= 0 && row_end <= resize_row && col_begin >= 0 && col_end <= resize_col
      % Crop with a bigger size
      rgb_crop   = rgb_train_image(row_begin:row_end, col_begin:col_end, :);
      label_crop = label_train_image(row_begin:row_end, col_begin:col_end, :);

      % Rotate img: (+) ccw, (-) cw
      rgb_crop   = imrotate(rgb_crop, orientation);
      label_crop = imrotate(label_crop, orientation);

      % Final Crop
      [row_size, col_size, ~] = size(rgb_crop);% Watch out! RGB!
      row_begin = row_size/2 - crop_size/2;
      row_end   = row_size/2 + crop_size/2 -1;

      col_begin = col_size/2 - crop_size/2;
      col_end   = col_size/2 + crop_size/2 -1;

      rgb_crop   = rgb_crop(row_begin:row_end, col_begin:col_end, :);
      label_crop = label_crop(row_begin:row_end, col_begin:col_end, :);

      % Save result
      save_name = strcat(workspace_path, 'train/rgb/', num2str(train_data_count,'%010i'),'_rgb.png');
      imwrite(rgb_crop, strcat(save_name));

      save_name = strcat(workspace_path, 'train/label/', num2str(train_data_count,'%010i'),'_label.png');
      imwrite(label_crop, strcat(save_name));

      train_data_count = train_data_count + 1;
    end
end
