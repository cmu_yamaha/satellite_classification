clc; clear all; close all; warning('off','all');

%% Hyper-parameter
workspace_path        = '/home/dkkim930122/research/air_lab/yamaha/global_classification/data/v2/';% Please change according to your workspace path
bn_threshold          = 1;% Binarize threshold
number_of_train_data  = 50000;
number_of_val_data    = 2000;
crop_size             = 224;% In pixel size (1 pixel corresponds to 0.3 meter)
train_row_start       = 1;
train_row_end         = 4599;
train_col_start       = 955;
train_col_end         = 2144; 
val_row_start         = 2500;
val_row_end           = 3099;
val_col_start         = 2800;
val_col_end           = 3399;

%% Make rgb_train_image, label_train_image & rgb_val_image, label_val_image
% Read data
rgb_image   = imread(strcat(workspace_path, 'rgb_image.png'));
label_image = imread(strcat(workspace_path, 'label_image.png'));

% Crop and make train data according to the crop parameters
rgb_train_image   = rgb_image(train_row_start:train_row_end, train_col_start:train_col_end, :);
label_train_image = label_image(train_row_start:train_row_end, train_col_start:train_col_end);

rgb_val_image   = rgb_image(val_row_start:val_row_end, val_col_start:val_col_end, :);
label_val_image = label_image(val_row_start:val_row_end, val_col_start:val_col_end);

% Show crop result
figure(); imshow(rgb_train_image);
figure(); imshow(label_train_image);
fuse_image = imfuse(rgb_train_image, label_train_image, 'falsecolor', 'Scaling', 'joint', 'ColorChannels', [1 2 0]); figure(); imshow(fuse_image)

figure(); imshow(rgb_val_image);
figure(); imshow(label_val_image);
fuse_image = imfuse(rgb_val_image, label_val_image, 'falsecolor', 'Scaling', 'joint', 'ColorChannels', [1 2 0]); figure(); imshow(fuse_image)

%% Make folder for saving data
mkdir(strcat(workspace_path, 'train'));
mkdir(strcat(workspace_path, 'train/rgb'));
mkdir(strcat(workspace_path, 'train/label'));

mkdir(strcat(workspace_path, 'val'));
mkdir(strcat(workspace_path, 'val/rgb'));
mkdir(strcat(workspace_path, 'val/label'));

%% Make train data
% NOTE: train data is collected at random location and random orientation
train_data_count = 1;
[resize_row, resize_col, ~] = size(rgb_train_image);

while train_data_count <= number_of_train_data
    % Generate random number
    row         = rand(1)*resize_row;
    col         = rand(1)*resize_col;
    orientation = rand(1)*360;

    % Set crop parameter
    bigger_crop_size = crop_size*sqrt(2);

    row_begin = row - bigger_crop_size/2;
    row_end   = row + bigger_crop_size/2;
    col_begin = col - bigger_crop_size/2;
    col_end   = col + bigger_crop_size/2;

    % Check margin
    if row_begin >= 0 && row_end <= resize_row && col_begin >= 0 && col_end <= resize_col
      % Crop with a bigger size
      rgb_crop   = rgb_train_image(row_begin:row_end, col_begin:col_end, :);
      label_crop = label_train_image(row_begin:row_end, col_begin:col_end, :);

      % Rotate img: (+) ccw, (-) cw
      rgb_crop   = imrotate(rgb_crop, orientation);
      label_crop = imrotate(label_crop, orientation);

      % Final Crop
      [row_size, col_size, ~] = size(rgb_crop);% Watch out! RGB!
      row_begin = row_size/2 - crop_size/2;
      row_end   = row_size/2 + crop_size/2 -1;

      col_begin = col_size/2 - crop_size/2;
      col_end   = col_size/2 + crop_size/2 -1;

      rgb_crop   = rgb_crop(row_begin:row_end, col_begin:col_end, :);
      label_crop = label_crop(row_begin:row_end, col_begin:col_end, :);

      % Save result
      save_name = strcat(workspace_path, 'train/rgb/', num2str(train_data_count,'%010i'),'_rgb.png');
      imwrite(rgb_crop, strcat(save_name));

      save_name = strcat(workspace_path, 'train/label/', num2str(train_data_count,'%010i'),'_label.png');
      imwrite(label_crop, strcat(save_name));

      train_data_count = train_data_count + 1;
    end
end

%% Make val data
% NOTE: val data is collected at random location and random orientation
val_data_count = 1;
[resize_row, resize_col, ~] = size(rgb_val_image);

while val_data_count <= number_of_val_data
    % Generate random number
    row         = rand(1)*resize_row;
    col         = rand(1)*resize_col;
    orientation = rand(1)*360;

    % Set crop parameter
    bigger_crop_size = crop_size*sqrt(2);

    row_begin = row - bigger_crop_size/2;
    row_end   = row + bigger_crop_size/2;
    col_begin = col - bigger_crop_size/2;
    col_end   = col + bigger_crop_size/2;

    % Check margin
    if row_begin >= 0 && row_end <= resize_row && col_begin >= 0 && col_end <= resize_col
      % Crop with a bigger size
      rgb_crop   = rgb_val_image(row_begin:row_end, col_begin:col_end, :);
      label_crop = label_val_image(row_begin:row_end, col_begin:col_end, :);

      % Rotate img: (+) ccw, (-) cw
      rgb_crop   = imrotate(rgb_crop, orientation);
      label_crop = imrotate(label_crop, orientation);

      % Final Crop
      [row_size, col_size, ~] = size(rgb_crop);% Watch out! RGB!
      row_begin = row_size/2 - crop_size/2;
      row_end   = row_size/2 + crop_size/2 -1;

      col_begin = col_size/2 - crop_size/2;
      col_end   = col_size/2 + crop_size/2 -1;

      rgb_crop   = rgb_crop(row_begin:row_end, col_begin:col_end, :);
      label_crop = label_crop(row_begin:row_end, col_begin:col_end, :);

      % Save result
      save_name = strcat(workspace_path, 'val/rgb/', num2str(val_data_count,'%010i'),'_rgb.png');
      imwrite(rgb_crop, strcat(save_name));

      save_name = strcat(workspace_path, 'val/label/', num2str(val_data_count,'%010i'),'_label.png');
      imwrite(label_crop, strcat(save_name));

      val_data_count = val_data_count + 1;
    end
end
