clc; clear all; close all; warning('off','all');

%% Hyper-parameter
workspace_path = '/home/dkkim930122/research/air_lab/yamaha/global_classification/data/v1/';% Please change this to according to your system
bn_threshold = 1;% Binarize threshold
number_of_crop = 25000;
crop_size = 224;% In pixel size (1 pixel corresponds to 0.3 meter)

%% Read Data
% It would be okay to use 'rgb_image.png' and 'label_image.png' instead of the below images.
% But somehow original code used this, so ... :)
rgb_image   = imread(strcat(workspace_path, 'HighResOrthography_2012.tif'));
label_image = imread(strcat(workspace_path, 'NAIP_2013_nov16_park_thresh_fix.png'));

%% Preprocess for label_image
% Convert label_image to grayscale
label_image = rgb2gray(label_image);

% Resize label_imate to match rgb_image
[resize_row, resize_col, ~] = size(rgb_image);
label_image = imresize(label_image, [resize_row, resize_col]);% Uses bicubic by default

% Binarize label_image
for row = 1:resize_row
    for col = 1:resize_col
        if label_image(row, col) > bn_threshold
            label_image(row, col) = 1;
        else
            label_image(row, col) = 0;
        end
    end
end
label_image = label_image * 255;

% Crop image
rgb_image = imcrop(rgb_image, [610, 460, 3850, 5350]);
label_image = imcrop(label_image, [610, 460, 3850, 5350]);

%% Test Region
test_row_begin = 1291;
test_row_end   = 1926;
test_col_begin = 2455;
test_col_end   = 3075;

test_rgb_image   = rgb_image(test_row_begin:test_row_end, test_col_begin:test_col_end, :); figure(); imshow(test_rgb_image);
test_label_image = label_image(test_row_begin:test_row_end, test_col_begin:test_col_end); figure(); imshow(test_label_image);

%% Crop and save image
[resize_row, resize_col, ~] = size(test_rgb_image);

for row = 1:50:resize_row-crop_size
    for col = 1:50:resize_col-crop_size
        current_rgb_image   = test_rgb_image(row:row+crop_size-1, col:col+crop_size-1, :);
        current_label_image = test_label_image(row:row+crop_size-1, col:col+crop_size-1);

        imwrite(current_rgb_image, strcat('row_', num2str(row), '_col_', num2str(col), '_rgb.png'))
        imwrite(current_label_image, strcat('row_', num2str(row), '_col_', num2str(col), '_label.png'))
    end
end
