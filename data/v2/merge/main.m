clear all; clc; close all;

%% Read label images by various source
label_image_by_bill   = imread('label_image_by_bill.png');
label_image_by_powei  = imread('label_image_by_powei.png');
label_image_by_dongki = imread('label_image_by_dongki.png');

%% Initialize merged label image
merged_label_image = zeros(6100, 4720);

%% Create merged label iamge
% Label image by bill
for row = 1:6100
    for col = 1:4720
        if label_image_by_bill(row, col) == 255
            merged_label_image(row, col) = 255;
        end
    end
end

% Label image by powei
for row = 1:6100
    for col = 1:4720
        if label_image_by_powei(row, col) == 255
            merged_label_image(row, col) = 255;
        end
    end
end

% Label image by dongki
for row = 1:6100
    for col = 1:4720
        if label_image_by_dongki(row, col) == 255
            merged_label_image(row, col) = 255;
        end
    end
end

%% Save result
rgb_image = imread('HighResOrthography_2012.tif');
fuse_image = imfuse(rgb_image, merged_label_image, 'falsecolor', 'Scaling', 'joint', 'ColorChannels', [1 2 0]); figure(); imshow(fuse_image)
imwrite(fuse_image, 'fuse_image.png');
imwrite(merged_label_image, 'merge_image.png')
