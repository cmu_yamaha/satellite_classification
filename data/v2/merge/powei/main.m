clear all; clc; close all;


%% Read Image and utm info
satellite_image = imread('HighResOrthography_2012.tif');
utm_info = worldfileread('HighResOrthography_2012.tfw');
[data_in_media, data_in_local] = data_list();

%%
[row_size, col_size, ~] = size(satellite_image);
row_prjection = []; col_projection = [];

%% data_in_media
path = '/home/dkkim930122/Downloads/';
for data_package_index = 1:length(data_in_local)
  data_in_local{data_package_index}

  % Loop through rosbag
  x = []; y = [];

  % Read odometry from rosbag
  bag = rosbag(strcat(path, data_in_local{data_package_index}));
  odom_message = select(bag, 'Topic', '/odom');
  odom_message = readMessages(odom_message);

  if length(odom_message) == 0
    odom_message = select(bag, 'Topic', '/span/pose');
    odom_message = readMessages(odom_message);
  end

  % Extract x and y information
  for odom_index = 1:length(odom_message)
    x = [x odom_message{odom_index}.Pose.Pose.Position.X];
    y = [y odom_message{odom_index}.Pose.Pose.Position.Y];
  end

  % Project onto Satellite image
  [r, c] = map2pix(utm_info, x, y);
  row_prjection = [row_prjection, r]; col_projection = [col_projection, c];
  save('projection_image.mat', 'row_prjection', 'col_projection');

  fig = figure(1);
  imshow(satellite_image); hold on;
  plot(col_projection, row_prjection, 'Marker', 'o', 'Color', [55/255, 94/255, 151/255], 'LineStyle', 'none', 'MarkerSize', 3);
  saveas(fig, 'projection_image.jpg', 'jpg'); close all;
end

%% data_in_media
path = '/media/dkkim930122/My Passport/data/yamaha_lidar_feature_data_collection/original/';
for data_package_index = 1:length(data_in_media)
  data_in_media{data_package_index}

  % Loop through rosbag
  x = []; y = [];

  % Read odometry from rosbag
  bag = rosbag(strcat(path, data_in_media{data_package_index}));
  odom_message = select(bag, 'Topic', '/odom');
  odom_message = readMessages(odom_message);

  if length(odom_message) == 0
    odom_message = select(bag, 'Topic', '/span/pose');
    odom_message = readMessages(odom_message);
  end

  % Extract x and y information
  for odom_index = 1:length(odom_message)
    x = [x odom_message{odom_index}.Pose.Pose.Position.X];
    y = [y odom_message{odom_index}.Pose.Pose.Position.Y];
  end

  % Project onto Satellite image
  [r, c] = map2pix(utm_info, x, y);
  row_prjection = [row_prjection, r]; col_projection = [col_projection, c];
  save('projection_image.mat', 'row_prjection', 'col_projection');

  fig = figure(1);
  imshow(satellite_image); hold on;
  plot(col_projection, row_prjection, 'Marker', 'o', 'Color', [55/255, 94/255, 151/255], 'LineStyle', 'none', 'MarkerSize', 3);
  saveas(fig, 'projection_image.jpg', 'jpg'); close all;
end

%%
load 'projection_image.mat'
row_projection = row_prjection;
col_projection = col_projection;

% Round and Take care of boundary
row_projection = round(row_projection);
col_projection = round(col_projection);

row_projection(row_projection < 1) = 2939;
row_projection(row_projection > row_size) = 2939;

col_projection(col_projection < 1) = 3161;
col_projection(col_projection > col_size) = 3161;

% image
projection_image = zeros(row_size, col_size);
for index = 1:length(col_projection)
  r = row_projection(index); c = col_projection(index);

  for index_1 = -4:4
    for index_2 = -4:4
      projection_image(r+index_1, c+index_2) = 1;
    end
  end
end

imshow(projection_image)
