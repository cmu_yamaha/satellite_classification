import json, cv2
import numpy as np
from scipy.misc import imread, imsave

rgb_image = imread('../HighResOrthography_2012.tif')

# Read .lif
with open('dongki_label.lif') as data_file:
    label = json.load(data_file)

# Vertices
hmm = []
for vertice_index in range(12):# where 8 corresponds to number 'x's in the .lif
    print vertice_index
    vertices = label["shapes"][vertice_index]["points"]
    vertices = np.asarray(vertices, dtype=np.int32)
    vertices = np.round(vertices)

    mask = np.zeros((rgb_image.shape[0], rgb_image.shape[1])); img = mask
    cv2.fillConvexPoly(mask, vertices, 1)
    mask = mask.astype(np.bool)
    out = np.zeros_like(img); out[mask] = img[mask]

    hmm.append(out)

label_image = np.zeros_like(img)
for channel in range(len(hmm)):
    current_img = hmm[channel]
    print np.unique(current_img.flatten())
    for row in range(6100):
        for col in range(4720):
            if current_img[row, col] == 1:
                label_image[row, col] = current_img[row, col]

imsave('label_image_by_dongki.png', label_image)
